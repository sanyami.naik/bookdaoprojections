package com;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class MainApplication {
    public static void main(String[] args) {
        Configuration configuration=new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();



        Employee employee=new Employee(1,"Sanyami");
        PartTimeEmployee partTimeEmployee=new PartTimeEmployee(2,"Aman",10,100);
        FullTimeEmployee fullTimeEmployee=new FullTimeEmployee(3,"Ayushman",10000,5,500);



        session.save(employee);
        session.save(partTimeEmployee);
        session.save(fullTimeEmployee);

        transaction.commit();
        session.close();

    }
}
