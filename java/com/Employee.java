package com;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Employee {
    int employeeId;
    String name;

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee(int employeeId, String name) {
        this.employeeId = employeeId;
        this.name = name;
    }
}

class PartTimeEmployee extends Employee{
    int noOfHours;
    int salaryPerHour;

    public int getNoOfHours() {
        return noOfHours;
    }

    public int getSalaryPerHour() {
        return salaryPerHour;
    }

    public void setSalaryPerHour(int salaryPerHour) {
        this.salaryPerHour = salaryPerHour;
    }

    public void setNoOfHours(int noOfHours) {
        this.noOfHours = noOfHours;
    }

    public PartTimeEmployee(int employeeId, String name, int noOfHours, int salaryPerHour) {
        super(employeeId, name);
        this.noOfHours = noOfHours;
        this.salaryPerHour = salaryPerHour;
    }
}


class FullTimeEmployee extends Employee{
    int fixedSalary;
    int noOfHours;

    public int getFixedSalary() {
        return fixedSalary;
    }

    public void setFixedSalary(int fixedSalary) {
        this.fixedSalary = fixedSalary;
    }

    public int getNoOfHours() {
        return noOfHours;
    }

    public void setNoOfHours(int noOfHours) {
        this.noOfHours = noOfHours;
    }

    public int getSalaryPerHour() {
        return salaryPerHour;
    }

    public void setSalaryPerHour(int salaryPerHour) {
        this.salaryPerHour = salaryPerHour;
    }

    int salaryPerHour;

    public FullTimeEmployee(int employeeId, String name, int fixedSalary,int noOfHours, int salaryPerHour) {
        super(employeeId, name);
        this.fixedSalary=fixedSalary;
        this.noOfHours = noOfHours;
        this.salaryPerHour = salaryPerHour;
    }
}
